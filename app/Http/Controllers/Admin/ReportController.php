<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Student;
use Illuminate\Database\QueryException;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reportStatus = $request->input('reportStatus');
        $reports = Report::when($reportStatus, function ($query, $reportStatus) {
            return $query->where('status', $reportStatus);
        })
            ->paginate(6);
        return view('admin.reports.index', ['report_list' => $reports, 'reportStatus' => $reportStatus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        return view('admin.reports.create', ['student_list' => $students]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'report_date' => 'required',
            'photo' => 'required|file',
            'report' => 'required',
            'status' => 'required',
            'student_id' => 'required',
        ]);

        $path = $request->photo->store('public/images');
        $data['photo'] = str_replace('public/', '', $path);

        Report::create($data);

        return redirect('admin/reports');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::find($id);
        return view('admin.reports.detail', ['report' => $report]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'status' => 'required',
            'photo' => 'nullable|file'
        ]);

        if (array_key_exists('photo', $data)) {
            $path = $request->photo->store('public/images');
            $data['photo'] = str_replace('public/', '', $path);
        }

        Report::where('id', $id)->update($data);

        return redirect('/admin/reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Report::destroy($id);
            return redirect('/admin/reports');
        } catch (QueryException $exc) {
            return redirect('/admin/reports')
                ->withErrors([
                    'msg' => 'Report'  .  $id .  'cannot be deleted because related with other entity'
                ]);
        }
    }
}
