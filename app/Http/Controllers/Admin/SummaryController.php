<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Response;
use Illuminate\Http\Request;

class SummaryController extends Controller
{
    public function responsesReport(Request $request)
    {
        $data = $request->validate([
            'start' => 'filled',
            'end' => 'filled',
        ]);

        if (array_key_exists('start', $data) && array_key_exists('end', $data)) {
            $responses = Response::where('responses_date', '>=', $data['start'])
                ->where('responses_date', '<=', $data['end'])->get();
            $start = $data['start'];
            $end = $data['end'];
        } else {
            $responses = Response::all();
            $start = '';
            $end = '';
        }

        return view('admin.responses.summary', [
            'response_list' => $responses, 'start' => $start, 'end' => $end
        ]);
    }
}
