<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Response;
use App\Models\Report;
use App\Models\User;
use Illuminate\Database\QueryException;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responses = Response::paginate(6);
        return view('admin.responses.index', ['response_list' => $responses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Report::all();
        $users = User::all();
        return view('admin.responses.create', ['report_list' => $reports], ['user_list' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'response_date' => 'required',
            'responses' => 'required',
            'report_id' => 'required',
            'staff_id' => 'required',
        ]);

        Response::create($data);

        return redirect('admin/responses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = Response::find($id);
        return view('admin.responses.detail', ['response' => $response]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'response_date' => 'required',
            'responses' => 'required',
        ]);

        Response::where('id', $id)->update($data);

        return redirect('/admin/responses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Response::destroy($id);
            return redirect('/admin/responses');
        } catch (QueryException $exc) {
            return redirect('/admin/responses')
                ->withErrors([
                    'msg' => 'Response'  .  $id .  'cannot be deleted because related with other entity'
                ]);
        }
    }
}
