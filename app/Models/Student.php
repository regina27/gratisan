<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'phone',
        'name',
        'grade',
        'user_id',
        'username',
        'password',
        'level',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function Reports()
    {
        return $this->hasMany(Report::class);
    }
}
