<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Report;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $user_1 = User::create([
            'username' => 'reginagrc12',
            'password' => Hash::make('Regina_123'),
            'level' => 'admin',
        ]);

        $user_2 = User::create([
            'username' => 'artagrc12',
            'password' => Hash::make('Arta_123'),
            'level' => 'staff',
        ]);

        $user_3 = User::create([
            'username' => 'agustinusgrc12',
            'password' => Hash::make('Agustinus_123'),
            'level' => 'student',
        ]);

        $student_3 = Student::create([
            'phone' => '082128478602',
            'name' => 'Agustinus Marulitua',
            'grade' => 'XII',
            'user_id' => $user_3->id,
        ]);

        $report_3 = Report::create([
            'report_date' => '2023-01-01',
            'photo' => '',
            'report' => 'Saya mendengar Bryan menghina Michael dengan kata-kata kasar',
            'status' => 'Completed',
            'student_id'  => $student_3->id,
        ]);

        $response_3 = Response::create([
            'report_id' => $report_3->id,
            'response_date' => '2023-01-05',
            'responses' => 'Terima kasih atas laporannya akan segera kami tindak lanjuti',
            'staff_id' => $user_2->id,
        ]);

        $user_4 = User::create([
            'username' => 'ameliagrc12',
            'password' => Hash::make('Amelia_123'),
            'level' => 'student',
        ]);

        $student_4 = Student::create([
            'phone' => '0895356012314',
            'name' => 'Amelia Venesa',
            'grade' => 'XII',
            'user_id' => $user_4->id,
        ]);

        $report_4 = Report::create([
            'report_date' => '2022-12-12',
            'photo' => '',
            'report' => 'Saya didorong oleh Maria, dan ditertawai oleh teman-temannya',
            'status' => 'Completed',
            'student_id'  => $student_4->id,
        ]);

        $response_4 = Response::create([
            'report_id' => $report_4->id,
            'response_date' => '2022-12-16',
            'responses' => 'Terima kasih atas laporannya akan segera kami tindak lanjuti',
            'staff_id' => $user_1->id,
        ]);

        $user_5 = User::create([
            'username' => 'catherinegrc12',
            'password' => Hash::make('Catherine_123'),
            'level' => 'student',
        ]);

        $student_5 = Student::create([
            'phone' => '082219950088',
            'name' => 'Catherine Chyntia Sulaeman',
            'grade' => 'XII',
            'user_id' => $user_5->id,
        ]);

        $report_5 = Report::create([
            'report_date' => '2023-02-02',
            'photo' => '',
            'report' => 'Saya dijauhi teman-teman secara tiba-tiba dan dalam jangka waktu yang panjang, sudah sekitar 1 minggu',
            'status' => 'Processed',
            'student_id'  => $student_5->id,
        ]);

        $response_5 = Response::create([
            'report_id' => $report_5->id,
            'response_date' => '2023-02-03',
            'responses' => 'Terima kasih atas laporannya akan segera kami tindak lanjuti',
            'staff_id' => $user_1->id,
        ]);

        $user_6 = User::create([
            'username' => 'christiangrc12',
            'password' => Hash::make('Christian_123'),
            'level' => 'student',
        ]);

        $student_6 = Student::create([
            'phone' => '089526755453',
            'name' => 'Christian Andrean',
            'grade' => 'XII',
            'user_id' => $user_6->id,
        ]);

        $report_6 = Report::create([
            'report_date' => '2023-03-05',
            'photo' => '',
            'report' => 'Levin menghasut saya untuk tidak menemani Hendri karena mereka sedang musuhan',
            'status' => 'New',
            'student_id'  => $student_6->id,
        ]);

        $response_6 = Response::create([
            'report_id' => $report_6->id,
            'response_date' => '2023-03-05',
            'responses' => 'Terima kasih atas laporannya akan segera kami tindak lanjuti',
            'staff_id' => $user_2->id,
        ]);

        $user_7 = User::create([
            'username' => 'christophergrc12',
            'password' => Hash::make('Christopher_123'),
            'level' => 'student',
        ]);

        $student_7 = Student::create([
            'phone' => '085720695788',
            'name' => 'Christopher Gunawan',
            'grade' => 'XII',
            'user_id' => $user_7->id,
        ]);

        $report_7 = Report::create([
            'report_date' => '2023-03-07',
            'photo' => '',
            'report' => 'Saya mendapat bukti chat dari Agustinus kalau Reymond menyebar foto saya kepada Bryan',
            'status' => 'Processed',
            'student_id'  => $student_7->id,
        ]);

        $response_7 = Response::create([
            'report_id' => $report_7->id,
            'response_date' => '2023-03-08',
            'responses' => 'Terima kasih atas laporannya akan segera kami tindak lanjuti',
            'staff_id' => $user_2->id,
        ]);

        $user_8 = User::create([
            'username' => 'dindagrc12',
            'password' => Hash::make('Dinda_123'),
            'level' => 'student',
        ]);

        $student_8 = Student::create([
            'phone' => '081311751724',
            'name' => 'Dinda Simbolon',
            'grade' => 'XII',
            'user_id' => $user_8->id,
        ]);

        $report_8 = Report::create([
            'report_date' => '2023-03-08',
            'photo' => '',
            'report' => 'Saya disuruh-suruh oleh Marshena, setiap hari disuruh untuk mengerjakan semua PRnya',
            'status' => 'New',
            'student_id'  => $student_8->id,
        ]);

        $response_8 = Response::create([
            'report_id' => $report_8->id,
            'response_date' => '2023-03-08',
            'responses' => 'Terima kasih atas laporannya akan segera kami tindak lanjuti',
            'staff_id' => $user_1->id,
        ]);

        $user_9 = User::create([
            'username' => 'graceangelinagrc12',
            'password' => Hash::make('Graceangelina_123'),
            'level' => 'student',
        ]);

        $student_9 = Student::create([
            'phone' => '089510118108',
            'name' => 'Grace Angelina',
            'grade' => 'XII',
            'user_id' => $user_9->id,
        ]);

        $report_9 = Report::create([
            'report_date' => '2023-01-08',
            'photo' => '',
            'report' => 'Karin memaki saya karena belum membayar SPP',
            'status' => 'Rejected',
            'student_id'  => $student_9->id,
        ]);

        $response_9 = Response::create([
            'report_id' => $report_9->id,
            'response_date' => '2023-01-12',
            'responses' => 'Terima kasih atas laporannya, saat ini laporan anda belum memenuhi kriteria, silahkan menghubungi petugas untuk konfirmasi',
            'staff_id' => $user_2->id,
        ]);

        $user_10 = User::create([
            'username' => 'gracekristianigrc12',
            'password' => Hash::make('Gracekristiani_123'),
            'level' => 'student',
        ]);
    }
}
