@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Reports Detail</h1>
        <form action="/staff/reports/{{ $report->id }}" method="POST" enctype="multipart/form-data" style="font-size: 18px;">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="report_date" class="form-label">Report Date</label>
                    <input type="date" class="form-control" id="report_date" name="report_date"
                        value="{{ $report->report_date }}" disabled>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo" accept="image/png,image/jpeg"
                        disabled>
                    <img src="{{ asset('storage/' . $report->photo) }}" style="width: 400px" alt="">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report" value="{{ $report->report }}"
                        disabled>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label class="form-label">Status</label>
                    <select name="status" class="form-select">
                        @foreach (['New', 'Processed', 'Completed', 'Rejected'] as $item)
                            <option value="{{ $item }}" {{ $report->status == $item ? 'selected' : 'New' }}>
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="student_id" class="form-label">Student Id</label>
                    <input type="text" class="form-control" id="student_id" name="student_id"
                        value="{{ $report->student_id }}" disabled>
                </div>
            </div>
            <button type="submit" class="btn btn-warning mt-4">Save</button>
            <button type="reset" class="btn btn-danger mt-4 ms-3">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
