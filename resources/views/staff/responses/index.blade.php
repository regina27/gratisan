@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Responses Data</h1>
        <p>{{ $response_list->links() }}</p>
        <a href="/staff/responses/create" class="btn btn-success mb-3" style="margin-left: 1000px; margin-top: -60px;">Create
            New</a>
        <table class="table" style="font-size: 18px;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Response Date</th>
                    <th>Responses</th>
                    <th>Report Id</th>
                    <th>Staff Id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $response->id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->responses }}</td>
                        <td>{{ $response->report_id }}</td>
                        <td>{{ $response->staff_id }}</td>
                        <td>
                            <a style="width:50px; height:50px;" href="/staff/responses/{{ $response->id }}"
                                class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                            <a style="width:50px; height:50px;" href="/staff/reports/{{ $response->id }}"
                                class="btn btn-info bi bi-search pt-3"></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
