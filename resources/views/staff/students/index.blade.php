@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Students Data</h1>
        <p>{{ $student_list->links() }}</p>
        <a href="/staff/students/create" class="btn btn-success mb-3" style="margin-left: 1000px; margin-top: -60px;">Create
            New</a>
        <table class="table" style="font-size: 18px;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Grade</th>
                    <th>Phone</th>
                    <th>User Id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>
                        <td>{{ $student->id }}</td>
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->grade }}</td>
                        <td>{{ $student->phone }}</td>
                        <td>{{ $student->user_id }}</td>
                        <td>
                            <a style="width:50px; height:50px;" href="/staff/students/{{ $student->id }}"
                                class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
