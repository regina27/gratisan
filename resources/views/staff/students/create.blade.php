@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Create Students</h1>
        <form action="/staff/students" method="POST" style="font-size: 18px;">
            @csrf
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label class="form-label">Grade</label>
                    <select name="grade" class="form-select">
                        @foreach (['VII', 'VIII', 'IX', 'X', 'XI', 'XII'] as $item)
                            <option value="{{ $item }}">
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="text" class="form-control" id="password" name="password">
                </div>
            </div>
            <button type="submit" class="btn btn-warning mt-4">Save</button>
            <button type="reset" class="btn btn-danger mt-4 ms-3">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
