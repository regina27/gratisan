@extends('app')

@section('content')
    <div class="container" style="font-size: 18px; font-family: Josefin Sans;">
        <h1 class="mt-4 mb-4">Student Details</h1>
        <form action="/staff/students/{{ $student->id }}" method="POST" style="font-size: 18px;">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $student->name }}"
                        disabled>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label class="form-label">Grade</label>
                    <select name="grade" class="form-select">
                        @foreach (['VII', 'VIII', 'IX', 'X', 'XI', 'XII'] as $item)
                            <option value="{{ $item }}" {{ $student->grade == $item ? 'selected' : '' }}>
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $student->phone }}">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="user_id" class="form-label">User Id</label>
                    <input type="text" class="form-control" id="user_id" name="user_id" value="{{ $student->user_id }}"
                        disabled>
                </div>
            </div>
            <button type="submit" class="btn btn-warning mt-4">Save</button>
            <button type="reset" class="btn btn-danger mt-4 ms-3">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
