@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">User Details</h1>
        <form action="/staff/users/{{ $user->id }}" method="POST" style="font-size: 18px;">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}"
                        disabled>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password" disabled>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label class="form-label">Level</label>
                    <select name="level" class="form-select" disabled>
                        @foreach (['admin', 'staff', 'student'] as $item)
                            <option value="{{ $item }}" {{ $user->level == $item ? 'selected' : '' }}>
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
