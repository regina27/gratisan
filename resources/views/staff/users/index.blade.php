<?php
$user = auth()->user()->level;
?>

@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Users Data</h1>
        <form action="/staff/users" method="GET" style="font-size: 18px;">
            <div class="row">
                <label for="userLevel" class="col-1 col-form-label"><b>Level</b></label>
                <div class="col-3">
                    <select name="userLevel" id="userLevel" class="form-select pt-2" style="font-size: 18px;">
                        @foreach (['student', 'staff', 'admin'] as $item)
                            <option value="{{ $item }}">
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-dark d-print-none">Search</button>
                </div>
                <div class="col-1 ms-1">
                    <a href="/staff/users" class="btn btn-primary d-print-none">Back</a>
                </div>
            </div>
        </form>
        <p style="margin-top:40px;">{{ $user_list->links() }}</p>
        <a href="/staff/users/create" class="btn btn-success" style="margin-left: 1000px; margin-top: -65px;">Create New</a>
        <table class="table" style="font-size: 18px; margin-top:30px;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>Level</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user_list as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->level }}</td>
                        <td>
                            <a style="width:50px; height:50px;" href="/staff/users/{{ $user->id }}"
                                class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
