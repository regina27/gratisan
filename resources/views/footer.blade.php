<footer class="site-footer">
    <div class="container d-print-none">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <img src="/img/logo-footer.png" alt="">
                <p class="bi bi-1-circle-fill mt-4"> Pelaporan Tindak Bullying</p>
                <p class="bi bi-2-circle-fill"> Edukasi Pencegahan Bullying</p>
                <p class="bi bi-3-circle-fill"> Tindak Lanjut Kasus Bullying</p>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Pages</h6>
                <ul class="footer-links mt-2">
                    <li><a href="">Home</a></li>
                    <li><a href="">About Bullying</a></li>
                    <li><a href="">Report History</a></li>
                </ul>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Social Media</h6>
                <ul class="footer-links mt-2">
                    <li><a class="bi bi-envelope-at-fill" href=""> reginasimarmata27@gmail.com</a></li>
                    <li><a class="bi bi-telephone-plus-fill" href=""> 012345678910</a></li>
                    <li><a class="bi bi-whatsapp" href=""> 012345678910</a></li>
                    <li><a class="bi bi-instagram" href=""> @ sua.regina27</a></li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
</footer>
