<html>

<head>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>

<body>
    <div class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-6 p-5 mb-3">
                    <img src="/img/login.png" style="width: 550px" alt="">
                </div>
                <div class="login-form col-6 p-5 mb-5">
                    <h1 class="text-center bi bi-person-fill">Login</h1>
                    <form action="/login" method="POST">
                        @csrf
                        <form>
                            <div class="form-outline mb-3">
                                <label for="username" class="form-label">Username</label>
                                <input type="text" class="form-control" id="username" name="username"
                                    placeholder="example: username1234">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <input type="password" class="form-control" id="password" name="password"
                                    placeholder="example: #passWord_135">
                            </div>
                            <button type="submit" class="btn btn-warning col-12 mb-3">Login</button>
                        </form>
                    </form>
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>
