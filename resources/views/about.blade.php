<?php
$level = auth()->user()->level;
?>

@extends('app')

@section('content')
    <div class="about-bullying">
        <div class="container ms-5">
            <h2 style="margin-top: 100px;">Bentuk-Bentuk Bullying</h2>
            <div class="row mt-5">
                <div class="col-6">
                    <h4 style="text-align: center">Physical Bullying</h4>
                    <p>Berupa pukulan, menendang, menampar, meludahi atau segala bentuk kekerasan yang menggunakan fisik.
                        Dampak buruk yang dirasakan korban physical bullying adalah:<br>
                        ➀ Menunjukkan ketakutan berlebih.<br>
                        ➁ Kurang bersemangat atau takut pergi ke sekolah.<br>
                        ➂ Membuang muka dan menunjukkan rasa takut kepada pelaku bullying.<br>
                        ➃ Kadang menangis sendiri (karena teringat pengalaman saat mendapat perundungan fisik).<br>
                        ➄ Mendapatkan luka di tubuh
                    </p>
                </div>
                <div class="col-6">
                    <img src="/img/physical-bullying.jpg" style="width: 430px" alt="">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-6">
                    <img src="/img/verbal-bullying.jpg" style="width: 400px" alt="">
                </div>
                <div class="col-6">
                    <h4 style="text-align: center">Verbal Bullying</h4>
                    <p>Berupa celaan, fitnah, atau penggunaan kata-kata yang tidak baik untuk menyakiti orang lain.
                        Beberapa perilaku verbal bullying adalah:<br>
                        ➀ Perkataan kasar dan tidak sopan.<br>
                        ➁ Menjadikan teman sebagai bahan lelucon dan di luar batas kewajaran.<br>
                        ➂ Meledek teman yang memiliki suatu kelemahan fisik atau karakter.<br>
                        ➃ Menertawakan seseorang secara berlebihan.
                    </p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-6">
                    <h4 style="text-align: center">Relational Bullying</h4>
                    <p>Berupa pengabaian, pengucilan, cibiran dan segala bentuk tindakan untuk mengasingkan seseorang dari
                        komunitasnya. Anak yang mengalami pengucilan biasanya akan lebih sering terlihat sendiri.
                        Ada beberapa penyebab seorang anak mengalami relational bullying, misalnya:<br>
                        ➀ Pelaku merasa iri dengan kelebihan yang dimiliki korban.<br>
                        ➁ Korban memang mengalami kesulitan atau kelemahan dalam bergaul dan bersosialisasi.<br>
                        ➂ Korban pernah atau suka melakukan suatu perilaku (kebiasaan) yang dipandang “aneh”.<br>
                    </p>
                </div>
                <div class="col-6">
                    <img src="/img/relational-bullying.png" style="width: 400px" alt="">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-6">
                    <img src="/img/cyber-bullying.jpg" style="width: 400px" alt="">
                </div>
                <div class="col-6">
                    <h4 style="text-align: center">Cyber Bullying</h4>
                    <p>Berupa celaan, fitnah, atau penggunaan kata-kata yang tidak baik untuk menyakiti orang lain.
                        Beberapa perilaku verbal bullying adalah:<br>
                        ➀ Ikut-ikutan teman<br>
                        ➁ Sulit berempati terhadap orang lain<br>
                        ➂ Ingin terlihat kuat di mata orang lain<br>
                        ➃ Upaya untuk mendapatkan popularitas<br>
                        ➄ Hubungan yang tidak baik dengan keluarga
                    </p>
                </div>
            </div>
            <div class="row" style="margin-top: 80px;">
                <iframe class="video" style="width: 500px; height: 290px; margin-left:30px;"
                    src="https://www.youtube.com/embed/86_uuX77hsc"></iframe>
                <iframe class="video" style="width: 500px; height: 290px; margin-left:20px;"
                    src="https://www.youtube.com/embed/TbcUA-4AYzc"></iframe>
            </div>
            <div class="row mt-3">
                <iframe class="video" style="width: 500px; height: 290px; margin-left:30px;"
                    src="https://www.youtube.com/embed/FuOTF_E8d_k"></iframe>
                <iframe class="video" style="width: 500px; height: 290px; margin-left:20px;"
                    src="https://www.youtube.com/embed/XQJASala1UY"></iframe>
            </div>
        </div>
        <div class="place mt-5">
            <div class="row">
                <div class="col-12 ms-5 mt-5">
                    <h2 style="color:#504489;">Bullying Dapat Terjadi Di Mana Saja</h2>
                </div>
            </div>
            <div class="row me-3 mt-2 ms-2">
                <div class="col-4">
                    <div class="card mb-2" style="height: 503px">
                        <div class="mt-4">
                            <h5>Rumah</h5>
                            <img src="\img\rumah.png" style="width: 200px;" alt="">
                            <p>
                                ➀ Mendiamkan kamu tanpa alasan yang jelas<br>
                                ➁ Menjadikan kamu sebagai lelucon yang menyakitkan hati<br>
                                ➂ Selalu menyalahkan kamu ketika hal buruk terjadi<br>
                                ➃ Mengajak anggota keluarga lain untuk memperlakukan kamu dengan tidak baik
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card" style="height: 503px">
                        <div class="mt-4">
                            <h5>Lingkungan Sekitar</h5>
                            <img src="\img\sekitar.png" style="width: 200px" alt="">
                            <p>
                                ➀ Melakukan pelecehan seksual<br>
                                ➁ Melakukan catcalling yang membuat resah<br>
                                ➂ Mempermalukan dan memaki kamu di depan umum<br>
                                ➃ Melakukan fitnah tanpa dasar<br>
                                ➄ Melakukan rasisme terhadap perbedaan
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card" style="height: 503px">
                        <div class="mt-4">
                            <h5>Sekolah</h5>
                            <img src="\img\sekolah.png" style="width: 200px" alt="">
                            <p>
                                ➀ Memerintah adik kelas dengan sesuka hati<br>
                                ➁ Melakukan pemalakan<br>
                                ➂ Melakukan tindakan konyol yang tak layak dilakukan<br>
                                ➃ Hukuman fisik untuk kesalahan yang tak jelas<br>
                                ➄ Mengolok-olok menggunakan nama orang tua<br>
                            </p>
                            @if ($level == 'student')
                                <a class="btn btn-light bi bi-arrow-right-circle" href="/student/form"
                                    style="width: 130px; padding-right: 10px;">
                                    Melapor</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
