<?php
$level = auth()->user()->level;
?>

<header class="p-3" id="navigation-bar">
    <div class="container d-flex align-items-center d-print-none">
        <a href="#" class="d-flex align-items-center me-3 text-decoration-none">
            <a href="/{{ $level }}/home"><img src="/img/logo-navbar.png" style="width:200px"></a>
        </a>
        <ul class="nav me-auto ms-3">
            <li><a href="/{{ $level }}/about" class="nav-link active">About Bullying</a></li>
            <li><a href="/{{ $level }}/home" class="nav-link active">Home</a></li>
            @if ($level == 'student')
                <li><a href="/{{ $level }}/form" class="nav-link active">Report Form</a></li>
            @endif
            @if ($level != 'student')
                <li><a href="/{{ $level }}/users" class="nav-link active">Users</a></li>
                <li><a href="/{{ $level }}/students" class="nav-link active">Students</a></li>
            @endif
            <li><a href="/{{ $level }}/reports" class="nav-link active">Reports</a></li>
            <li><a href="/{{ $level }}/responses" class="nav-link active">Responses</a></li>
        </ul>
        <div class="dropdown">
            <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                <i class="bi bi-person-circle" style="font-size: 25px"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#" class="dropdown-item">Halo, {{ auth()->user()->username }}</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a href="/logout" class="dropdown-item">Log out</a></li>
            </ul>
        </div>
    </div>
</header>
