<html>

<head>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>

<body>
    <div class="first-page" style="background-image: url(/img/firstpage.png); background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-7">
                    <img src="/img/logo-navbar.png" style="margin-top: 40px; margin-left:10px; width:200px" alt="">
                    <h1 style="margin-top: 55px; font-size: 100px; font-family: Josefin Sans; color:white;">Gratisan
                    </h1>
                    <h3 style="font-size: 40px; font-family: Josefin Sans; color: #fed56d; margin-top:-20px;">Gracia
                        Antisipasi
                        Perundungan</h3>
                    <h5 style="font-size: 30px; font-family: Josefin Sans; color:white; margin-top:30px;">Menjadi
                        platform para
                        siswa<br>melaporkan perundungan<br>secara gratis
                    </h5>
                </div>
                <div class="col-5 p-5 mb-3">
                    <a href="/login" class="btn btn-light"
                        style="margin-left: 300px; width:100px; border-width:4px; border-radius:15 px; font-weight: bold;">Login</a>
                    <img src="/img/smile.svg" style="width: 400px; margin-top:40px;" alt="">
                </div>

            </div>
        </div>
    </div>
    <script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>
