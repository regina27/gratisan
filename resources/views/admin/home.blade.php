<?php
$level = auth()->user()->level;
?>
@extends('app')

@section('content')
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <div class="row mt-3 mb-3"
        style="background-color: white; border-radius: 40px; padding:20px; color: black; font-family: Josefin Sans;">
        <div class="col-5 ms-5 me-5 mt-2" style="background-color:rgb(122, 223, 120); padding:20px;">
            <h3>Visi Aplikasi</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, itaque quaerat, placeat
                similique mollitia corrupti earum quae vel ullam fugit explicabo repellat! Dolore quod quos
                recusandae voluptatum ut accusamus fuga blanditiis minus dolores nisi ea assumenda exercitationem
                atque error voluptatem tempora mollitia maiores distinctio voluptate earum, reprehenderit itaque
                excepturi ducimus qui. Deserunt tempore quaerat ut natus, aspernatur consequatur consequuntur quo!
            </p>
        </div>
        <div class="col-5 ms-5 mt-2" style="background-color:rgb(120, 182, 223); padding:20px;">
            <h3>Misi Aplikasi</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, itaque quaerat, placeat
                similique mollitia corrupti earum quae vel ullam fugit explicabo repellat! Dolore quod quos
                recusandae voluptatum ut accusamus fuga blanditiis minus dolores nisi ea assumenda exercitationem
                atque error voluptatem tempora mollitia maiores distinctio voluptate earum, reprehenderit itaque
                excepturi ducimus qui. Deserunt tempore quaerat ut natus, aspernatur consequatur consequuntur quo!
            </p>
        </div>
    </div>
    <div class="team" style="background-color: #8cc7ff; padding-top:30px; font-family: Josefin Sans;">
        <div class="row mt-4 mb-3" style="margin-left: 35px; color:#504489;">
            <h1>Kenalan dengan Tim Gratisan</h1>
        </div>
        <div class="row ms-5 pb-4" style="font-size:30px;">
            <div class="col-3"
                style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; width:370px;">
                <img src="\img\male.png" style="width: 70px; margin-right:20px;" alt=""><b>Asep</b>
                <p style="font-size: 15px; margin-top:10px;">Saya pernah mengalami bullying saat duduk di bangku SMP, maka
                    dari itu saya mau membantu setiap korban bullying agar setiap tindak perundungan di sekolah bisa bersih
                    tuntas.</p>
            </div>
            <div class="col-3"
                style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; margin-left:10px; width:370px;">
                <img src="\img\female.png" style="width: 70px; margin-right:20px;" alt=""><b>Regina</b>
                <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim
                    impedit hic eligendi quas.</p>
            </div>
            <div class="col-3"
                style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; margin-left:10px; width:370px;">
                <img src="\img\female.png" style="width: 70px; margin-right:20px;" alt=""><b>Luna</b>
                <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Tenetur culpa repellendus aliquid voluptatem eos, dolores error, dolorem voluptatum, deserunt eaque
                    magni quo.</p>
            </div>
        </div>
        <div class="row ms-5 pb-4" style="font-size:30px;">
            <div class="col-3"
                style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; width:370px;">
                <img src="\img\male.png" style="width: 70px; margin-right:20px;" alt=""><b>Harry</b>
                <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas
                    voluptas consectetur molestias quisquam praesentium impedit tempora ratione eum. Nobis tempora eos
                    quaerat facilis.</p>
            </div>
            <div class="col-3"
                style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; margin-left:10px; width:370px;">
                <img src="\img\male.png" style="width: 70px; margin-right:20px;" alt=""><b>Ilham</b>
                <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius
                    aperiam sint odio quam beatae mollitia incidunt nostrum sapiente minima.</p>
            </div>
            <div class="col-3"
                style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; margin-left:10px; width:370px;">
                <img src="\img\female.png" style="width: 70px; margin-right:20px;" alt=""><b>Arta</b>
                <p style="font-size: 15px; margin-top:10px;">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Dolores natus consequatur ea minima obcaecati eos iusto cupiditate labore culpa non deserunt atque,
                    velit accusantium!</p>
            </div>
        </div>
    </div>
    <div class="place" style="font-family: Josefin Sans;">
        <div class="row">
            <div class="col-12 ms-5 mt-5">
                <h2 style="color:#504489;">Bullying Dapat Terjadi Di Mana Saja</h2>
            </div>
        </div>
        <div class="row me-3 mt-2 ms-2">
            <div class="col-4">
                <div class="card mb-2" style="height: 503px">
                    <div class="mt-4">
                        <h5>Rumah</h5>
                        <img src="\img\rumah.png" style="width: 200px;" alt="">
                        <p>
                            ➀ Mendiamkan kamu tanpa alasan yang jelas<br>
                            ➁ Menjadikan kamu sebagai lelucon yang menyakitkan hati<br>
                            ➂ Selalu menyalahkan kamu ketika hal buruk terjadi<br>
                            ➃ Mengajak anggota keluarga lain untuk memperlakukan kamu dengan tidak baik
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" style="height: 503px">
                    <div class="mt-4">
                        <h5>Lingkungan Sekitar</h5>
                        <img src="\img\sekitar.png" style="width: 200px" alt="">
                        <p>
                            ➀ Melakukan pelecehan seksual<br>
                            ➁ Melakukan catcalling yang membuat resah<br>
                            ➂ Mempermalukan dan memaki kamu di depan umum<br>
                            ➃ Melakukan fitnah tanpa dasar<br>
                            ➄ Melakukan rasisme terhadap perbedaan
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="mt-4">
                        <h5>Sekolah</h5>
                        <img src="\img\sekolah.png" style="width: 200px" alt="">
                        <p>
                            ➀ Memerintah adik kelas dengan sesuka hati<br>
                            ➁ Melakukan pemalakan<br>
                            ➂ Melakukan tindakan konyol yang tak layak dilakukan<br>
                            ➃ Hukuman fisik untuk kesalahan yang tak jelas<br>
                            ➄ Mengolok-olok menggunakan nama orang tua<br>
                        </p>
                        <a class="btn btn-light bi bi-arrow-right-circle" href="/student/form"
                            style="width: 130px; padding-right: 10px;">
                            Melapor</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
