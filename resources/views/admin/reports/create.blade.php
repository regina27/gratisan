@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Create Reports</h1>
        <form action="/admin/reports/" method="POST" enctype="multipart/form-data" style="font-size: 18px;">
            @csrf
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="report_date" class="form-label">Report Date</label>
                    <input type="date" class="form-control" id="report_date" name="report_date">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo" accept="image/png,image/jpeg">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label class="form-label">Status</label>
                    <select name="status" class="form-select">
                        @foreach (['New', 'Processed', 'Completed', 'Rejected'] as $item)
                            <option value="{{ $item }}">
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label class="form-label">Student Id</label>
                    <select name="student_id" id="form-select">
                        @foreach ($student_list as $student)
                            <option value="{{ $student->id }}">{{ $student->id }} - {{ $student->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-warning mt-4">Save</button>
            <button type="reset" class="btn btn-danger mt-4 ms-3">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
