@extends('app')

@section('content')
    <div class="container"style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Reports Data</h1>
        <form action="/admin/reports" method="GET" style="font-size: 18px;">
            <div class="row">
                <label for="reportStatus" class="col-1 col-form-label"><b>Status</b></label>
                <div class="col-3">
                    <select name="reportStatus" id="reportStatus" class="form-select pt-2" style="font-size: 18px;">
                        @foreach (['New', 'Processed', 'Completed', 'Rejected'] as $item)
                            <option value="{{ $item }}">
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-dark d-print-none">Search</button>
                </div>
                <div class="col-1 ms-1">
                    <a href="/admin/reports" class="btn btn-primary d-print-none">Back</a>
                </div>
            </div>
        </form>
        <p style="margin-top:40px;">{{ $report_list->links() }}</p>
        <a href="/admin/reports/create" class="btn btn-success mb-3" style="margin-left: 1000px; margin-top: -60px;">Create
            New</a>
        <table class="table" style="font-size: 18px; margin-top:30px;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Report Date</th>
                    <th>Photo</th>
                    <th>Report</th>
                    <th>Status</th>
                    <th>Student Id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($report_list as $report)
                    <tr>
                        <td>{{ $report->id }}</td>
                        <td>{{ $report->report_date }}</td>
                        <td><img src="{{ asset('storage/' . $report->photo) }}" style="width: 150px" alt=""></td>
                        <td>{{ $report->report }}</td>
                        <td>{{ $report->status }}</td>
                        <td>{{ $report->student_id }}</td>
                        <td>
                            <a style="width:50px; height:50px;" href="/admin/reports/{{ $report->id }}"
                                class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                            <a style="width:50px; height:50px;" href="/admin/responses/{{ $report->id }}"
                                class="btn btn-info bi bi-search pt-3"></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
