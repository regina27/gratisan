<?php
$user = auth()->user()->level;
?>
@extends('app')

@section('content')
    <div class="container mt-4" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Users Data</h1>
        <form action="/admin/users" method="GET" style="font-size: 18px;">
            <div class="row">
                <label for="userLevel" class="col-1 col-form-label"><b>Level</b></label>
                <div class="col-3">
                    <select name="userLevel" id="userLevel" class="form-select pt-2" style="font-size: 18px;">
                        @foreach (['student', 'staff', 'admin'] as $item)
                            <option value="{{ $item }}">
                                {{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-dark d-print-none">Search</button>
                </div>
                <div class="col-1 ms-1">
                    <a href="/admin/users" class="btn btn-primary d-print-none">Back</a>
                </div>
            </div>
        </form>
        <p style="margin-top:40px;">{{ $user_list->links() }}</p>
        <a href="/admin/users/create" class="btn btn-success mb-3" style="margin-left: 1000px; margin-top: -65px;">Create
            New</a>
        <table class="table" style="font-size: 18px;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>Level</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user_list as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->level }}</td>
                        <td>
                            <a style="width:50px; height:50px;" href="/admin/users/{{ $user->id }}"
                                class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                            <a style="width:50px; height:50px;" href="#" class="btn btn-danger bi bi-trash3 pt-3"
                                data-bs-toggle="modal" data-bs-target="#modal-{{ $user->id }}"></a>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $user->id }}" tabindex="-1" style="font-size: 18px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Confirm</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>User with ID {{ $user->id }} will be deleted.</p>
                                    <p>Are you sure want to delete it?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/users/{{ $user->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        <button type="button" class="btn btn-success"
                                            data-bs-dismiss="modal">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
