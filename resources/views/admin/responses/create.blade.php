<?php
$level = auth()->user()->level;
?>

@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Create Responses</h1>
        <form action="/admin/responses/" method="POST" style="font-size: 18px;">
            @csrf
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="response_date" class="form-label">Response Date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="responses" class="form-label">Responses</label>
                    <input type="text" class="form-control" id="responses" name="responses">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label class="form-label">Report_Id</label>
                    <select name="report_id" id="form-select">
                        @foreach ($report_list as $report)
                            <option value="{{ $report->id }}">{{ $report->id }} - {{ $report->report }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label class="form-label">Staff_Id</label>
                    <select name="staff_id" id="form-select">
                        @foreach ($user_list as $user)
                            <option value="{{ $user->id }}">{{ $user->id }} - {{ $user->username }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-warning">Save</button>
            <button type="reset" class="btn btn-danger">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
