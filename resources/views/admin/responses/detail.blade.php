@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Responses Detail</h1>
        <form action="/admin/responses/{{ $response->id }}" method="POST" style="font-size: 18px;">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="response_date" class="form-label">Response Date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date"
                        value="{{ $response->response_date }}">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="responses" class="form-label">Responses</label>
                    <input type="text" class="form-control" id="responses" name="responses"
                        value="{{ $response->responses }}">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="report_id" class="form-label">Report Id</label>
                    <input type="text" class="form-control" id="report_id" name="report_id"
                        value="{{ $response->report_id }}" disabled>
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="staff_id" class="form-label">Staff Id</label>
                    <input type="text" class="form-control" id="staff_id" name="staff_id"
                        value="{{ $response->staff_id }}" disabled>
                </div>
            </div>
            <button type="submit" class="btn btn-warning mt-4">Save</button>
            <button type="reset" class="btn btn-danger mt-4 ms-3">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
