@extends('app')

@section('content')
    <div class="container"style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Reponses Report</h1>
        <form action="/admin/responses-report" method="GET">
            <div class="row">
                <label for="start" class="col-1 col-form-label">From</label>
                <div class="col-3">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">
                </div>
                <label for="end" class="col-1 col-form-label">To</label>
                <div class="col-3">
                    <input type="date" class="form-control" id="end" name="end" value="{{ $end }}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success d-print-none">Search</button>
                </div>
            </div>
        </form>
        <button type="button" class="btn btn-info d-print-none" style="margin-left: 1000px; margin-top:-55px;"
            onclick="window.print()">
            Print
        </button>
        <table class="table mt-5">
            <thead>
                <tr>
                    <th>Report Id</th>
                    <th>Name</th>
                    <th>Grade</th>
                    <th>Phone</th>
                    <th>Report Date</th>
                    <th>Report</th>
                    <th>Status</th>
                    <th>Response Date</th>
                    <th>Staff Id</th>
                    <th>Response</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $response->report_id }}</td>
                        <td>{{ $response->report->student->name }}</td>
                        <td>{{ $response->report->student->grade }}</td>
                        <td>{{ $response->report->student->phone }}</td>
                        <td>{{ $response->report->report_date }}</td>
                        <td>{{ $response->report->report }}</td>
                        <td>{{ $response->report->status }}</td>
                        <td>{{ $response->responses_date }}</td>
                        <td>{{ $response->staff_id }}</td>
                        <td>{{ $response->responses }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
