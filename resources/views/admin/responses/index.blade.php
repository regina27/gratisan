@extends('app')

@section('content')
    <div class="container mt-4" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Responses Data</h1>
        <p>{{ $response_list->links() }}</p>
        <a href="/admin/responses-report" class="btn btn-dark bi bi-printer mb-3 pt-2"
            style="margin-left: 950px; margin-top: -60px; width:40px; height:40px;"></a>
        <a href="/admin/responses/create" class="btn btn-success mb-3" style="margin-left: 1000px; margin-top: -60px;">Create
            New</a>
        <table class="table" style="font-size: 18px;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Responses Date</th>
                    <th>Responses</th>
                    <th>Report Id</th>
                    <th>Staff Id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $response->id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->responses }}</td>
                        <td>{{ $response->report_id }}</td>
                        <td>{{ $response->staff_id }}</td>
                        <td>
                            <a style="width:50px; height:50px;" href="/admin/responses/{{ $response->id }}"
                                class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                            <a style="width:50px; height:50px;" href="#" class="btn btn-danger bi bi-trash3 pt-3"
                                data-bs-toggle="modal" data-bs-target="#modal-{{ $response->id }}"></a>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $response->id }}" tabindex="-1" style="font-size: 18px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Confirm</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>Response with ID {{ $response->id }} will be deleted.</p>
                                    <p>Are you sure want to delete it?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/responses/{{ $response->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        <button type="button" class="btn btn-success"
                                            data-bs-dismiss="modal">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
