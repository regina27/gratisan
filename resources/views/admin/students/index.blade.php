@extends('app')

@section('content')
    <div class="container mt-4" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-5">Students Data</h1>
        <p>{{ $student_list->links() }}</p>
        <a href="/admin/students/create" class="btn btn-success mb-3" style="margin-left: 1000px; margin-top: -60px;">Create
            New</a>
        <table class="table" style="font-size: 18px;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Grade</th>
                    <th>Phone</th>
                    <th>User Id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>
                        <td>{{ $student->id }}</td>
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->grade }}</td>
                        <td>{{ $student->phone }}</td>
                        <td>{{ $student->user_id }}</td>
                        <td>
                            <a style="width:50px; height:50px;" href="/admin/students/{{ $student->id }}"
                                class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                            <a style="width:50px; height:50px;" href="#" class="btn btn-danger bi bi-trash3 pt-3"
                                data-bs-toggle="modal" data-bs-target="#modal-{{ $student->id }}"></a>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-{{ $student->id }}" tabindex="-1" style="font-size: 18px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Confirm</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <p>Student with ID {{ $student->id }} will be deleted.</p>
                                    <p>Are you sure want to delete it?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="/admin/students/{{ $student->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        <button type="button" class="btn btn-success"
                                            data-bs-dismiss="modal">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
