@extends('app')

@section('content')
    <div class="container" style="font-size: 18px; font-family: Josefin Sans;">
        <h1 class="mt-4 mb-4">Report Details</h1>
        <form action="/student/reports/{{ $report->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-6 mb-3">
                    <label for="report_date" class="form-label">Report Date</label>
                    <input type="date" class="form-control" id="report_date" name="report_date"
                        value="{{ $report->report_date }}" disabled>
                </div><br>
                <div class="col-6 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo" accept="image/png,image/jpeg"
                        disabled>
                    <img src="{{ asset('storage/' . $report->photo) }}" style="width: 400px" alt="">
                </div><br>
                <div class="col-6 mb-3">
                    <label for="report" class="form-label">Report</label>
                    <input type="text" class="form-control" id="report" name="report" value="{{ $report->report }}"
                        disabled>
                </div><br>
                <div class="col-6 mb-3">
                    <label for="status" class="form-label">Status</label>
                    <input type="text" class="form-control" id="status" name="status" value="{{ $report->status }}"
                        disabled>
                </div><br>
            </div>
        </form>
    </div>
@endsection
