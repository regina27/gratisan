@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-4">Reports Data</h1>
        <table class="table" style="font-size: 18px;">
            <thead>
                <tr>
                    <th>Report Date</th>
                    <th>Photo</th>
                    <th>Report</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($report_list as $report)
                    <tr>
                        <td>{{ $report->report_date }}</td>
                        <td><img src="{{ asset('storage/' . $report->photo) }}" style="width: 150px" alt=""></td>
                        <td>{{ $report->report }}</td>
                        <td>{{ $report->status }}</td>
                        <td>
                            <a style="width:50px; height:50px;" href="/student/reports/{{ $report->id }}"
                                class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                            <a style="width:50px; height:50px;" href="/student/responses/{{ $report->id }}"
                                class="btn btn-info bi bi-search pt-3"></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
