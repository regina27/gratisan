@extends('app')

@section('content')
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <div class="form-page">
        <div class="container"><br><br>
            <h1>Formulir Pengaduan</h1>
            <div class="row">
                <div class="col-4">
                    <img src="/img/Solidarity.svg" alt="" style="width: 500px; margin-top: 70px;">
                </div>
                <div class="col-8">
                    <form action="/student/reports" method="POST" enctype="multipart/form-data">
                        @csrf
                        <form>
                            <img src="/img/note.png" alt="" width="200px">
                            <label for="report_date" class="form-label mt-4">Tanggal Laporan</label>
                            <input type="date" class="form-control" id="report_date" name="report_date"
                                placeholder="Report Date">
                            <label for="report" class="form-label mt-3">Laporan</label>
                            <textarea type="report" class="form-control" id="report" name="report" placeholder="Report"></textarea>
                            <label for="photo" class="form-label mt-3">Foto Bukti</label>
                            <input type="file" class="form-control" id="photo" name="photo"
                                accept="image/png,image/jpeg">
                            <input type="submit" class="submit btn btn-warning mt-4 mb-3" value="Submit">
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <p class="text-danger">{{ $error }}</p>
                                @endforeach
                            @endif
                        </form>
                    </form>
                </div>
            </div>
            <style>
                form {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    padding: 20px;
                    background-color: #ffd0d0;
                    border-radius: 20px;
                    box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.5);
                    max-width: 500px;
                    margin: auto;
                }
            </style>
        </div>
        <div class="team" style="background-color: #8cc7ff; padding-top:30px; margin-top:100px;">
            <div class="row mt-4 mb-3" style="margin-left: 35px; color:#504489;">
                <h1>Review Layanan</h1>
            </div>
            <div class="row ms-5 pb-4" style="font-size:30px;">
                <div class="col-3"
                    style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; width:370px;">
                    <img src="\img\male.png" style="width: 70px; margin-right:20px;" alt=""><b>Anonim</b>
                    <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                        Enim, aperiam praesentium. Quia saepe architecto aliquam quidem, nemo aspernatur assumenda porro!
                    </p>
                </div>
                <div class="col-3"
                    style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; margin-left:10px; width:370px;">
                    <img src="\img\female.png" style="width: 70px; margin-right:20px;" alt=""><b>Anonim</b>
                    <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit amet consectetur adipisicing
                        elit. Enim
                        impedit hic eligendi quas.</p>
                </div>
                <div class="col-3"
                    style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; margin-left:10px; width:370px;">
                    <img src="\img\female.png" style="width: 70px; margin-right:20px;" alt=""><b>Anonim</b>
                    <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit amet consectetur adipisicing
                        elit.
                        Tenetur culpa repellendus aliquid voluptatem eos, dolores error, dolorem voluptatum, deserunt
                        eaque
                        magni quo.</p>
                </div>
            </div>
            <div class="row ms-5 pb-4" style="font-size:30px;">
                <div class="col-3"
                    style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; width:370px;">
                    <img src="\img\male.png" style="width: 70px; margin-right:20px;" alt=""><b>Anonim</b>
                    <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit amet consectetur adipisicing
                        elit. Quas
                        voluptas consectetur molestias quisquam praesentium impedit tempora ratione eum. Nobis tempora
                        eos
                        quaerat facilis.</p>
                </div>
                <div class="col-3"
                    style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; margin-left:10px; width:370px;">
                    <img src="\img\male.png" style="width: 70px; margin-right:20px;" alt=""><b>Anonim</b>
                    <p style="font-size: 15px; margin-top:10px;">Lorem ipsum dolor sit amet consectetur adipisicing
                        elit. Eius
                        aperiam sint odio quam beatae mollitia incidunt nostrum sapiente minima.</p>
                </div>
                <div class="col-3"
                    style="background-color: #70a5d696; border-radius: 20px; padding-top: 10px; padding-bottom:10px; margin-left:10px; width:370px;">
                    <img src="\img\female.png" style="width: 70px; margin-right:20px;" alt=""><b>Anonim</b>
                    <p style="font-size: 15px; margin-top:10px;">Lorem ipsum, dolor sit amet consectetur adipisicing
                        elit.
                        Dolores natus consequatur ea minima obcaecati eos iusto cupiditate labore culpa non deserunt
                        atque,
                        velit accusantium!</p>
                </div>
            </div>
        </div>
    </div>
@endsection
