@extends('app')

@section('content')
    <div class="container" style="font-size: 18px; font-family: Josefin Sans;">
        <h1 class="mt-4 mb-4">Response Details</h1>
        <form action="/student/responses/{{ $response->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-6">
                    <label for="response_date" class="form-label">Response Date</label>
                    <input type="date" class="form-control" id="response_date" name="response_date"
                        value="{{ $response->response_date }}" disabled>
                </div>
            </div><br>
            <div class="row flex-column">
                <div class="col-6">
                    <label for="responses" class="form-label">Responses</label>
                    <input type="text" class="form-control" id="responses" name="responses"
                        value="{{ $response->responses }}" disabled>
                </div>
            </div><br>
            <div class="row flex-column">
                <div class="col-6">
                    <label for="report_id" class="form-label">Report Id</label>
                    <input type="text" class="form-control" id="report_id" name="report_id"
                        value="{{ $response->report_id }}" disabled>
                </div>
            </div><br>
            <div class="row flex-column">
                <div class="col-6">
                    <label for="staff_id" class="form-label">Staff Id</label>
                    <input type="text" class="form-control" id="staff_id" name="staff_id"
                        value="{{ $response->staff_id }}" disabled>
                </div>
            </div><br>
        </form>
    </div>
@endsection
