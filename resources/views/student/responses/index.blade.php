@extends('app')

@section('content')
    <div class="container" style="font-family: Josefin Sans;">
        <h1 class="mt-4 mb-4">Responses Data</h1>
        <table class="table" style="font-size: 18px;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Report Id</th>
                    <th>Responses Date</th>
                    <th>Responses</th>
                    <th>Staff Id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    @if ($response->report->student->user->id == auth()->user()->id)
                        <tr>
                            <td>{{ $response->id }}</td>
                            <td>{{ $response->report_id }}</td>
                            <td>{{ $response->response_date }}</td>
                            <td>{{ $response->responses }}</td>
                            <td>{{ $response->staff_id }}</td>
                            <td>
                                <a style="width:50px; height:50px;" href="/student/responses/{{ $response->id }}"
                                    class="btn btn-warning bi bi-pencil-square pt-3"></a><br><br>
                                <a style="width:50px; height:50px;" href="/student/reports/{{ $response->id }}"
                                    class="btn btn-info bi bi-search pt-3"></a>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
